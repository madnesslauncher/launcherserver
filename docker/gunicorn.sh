#!/bin/sh

# collect static files
python manage.py collectstatic --noinput
# make sure to migrate
python manage.py migrate --noinput

# run gunicorn
#gunicorn -b 0.0.0.0:5000 cp_parsers.wsgi --workers 3 $*
gunicorn -b unix:/gunicorn_socket/socket launcher_base_server.wsgi --workers 3 $*
