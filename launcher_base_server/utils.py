import base64
import datetime
import hashlib
import json

from launcher_base_server import settings

from Cryptodome.Hash import SHA1
from Cryptodome.Signature import PKCS1_v1_5
from Cryptodome.PublicKey import RSA

# from base64 import b64decode


def sign(message: str, private_key_str: str) -> base64:
    # private_key = RSA.importKey(settings.signature_private_key)
    # chipher = PKCS1_v1_5.new(private_key)
    # signature = chipher.encrypt(json.dumps(textures).encode("utf8"))
    # signature = base64.b64encode(signature)
    # print(signature)

    priv_key = RSA.importKey(private_key_str)
    h = SHA1.new(message.encode('utf-8'))
    signature = PKCS1_v1_5.new(priv_key).sign(h)
    print("Result w_o decode: ", base64.b64encode(signature))
    result = base64.b64encode(signature)
    return result


def construct_offline_player_uuid(username):
    string = "OfflinePlayer:" + username
    cur_hash = hashlib.md5(string.encode('utf-8')).digest()
    byte_array = [byte for byte in cur_hash]
    # set the version to 3 -> Name based md5 hash
    byte_array[6] = cur_hash[6] & 0x0f | 0x30
    # IETF variant
    byte_array[8] = cur_hash[8] & 0x3f | 0x80

    hash_modified = bytes(byte_array)
    offline_player_uuid = hash_modified.hex()

    return offline_player_uuid


def add_uuid_stripes(string):
    string_striped = (
        string[:8] + '-' +
        string[8:12] + '-' +
        string[12:16] + '-' +
        string[16:20] + '-' +
        string[20:]
    )
    return string_striped


def generate_info_about_user(user, request, need_signature=True):
    print((datetime.datetime.now() - datetime.datetime(1970, 1, 1, 0, 0, 0)).seconds * 1000)
    textures = {
        "timestamp": (datetime.datetime.now() - datetime.datetime(1970, 1, 1, 0, 0, 0)).seconds * 1000,
        "profileId": str(user.uuid).replace("-", ""),
        "profileName": user.nickname,
        "textures": {
            "SKIN": {
                "url": request.build_absolute_uri(user.skin.url) if user.skin else ""
            },
            "CAPE": {
                "url": request.build_absolute_uri(user.cloak.url) if user.cloak else ""
            }
        }
    }
    print(textures)
    texturesb64 = base64.b64encode(json.dumps(textures).encode('utf-8'))

    signature = sign(json.dumps(textures), settings.signature_private_key)

    print(signature)
    answer = {
                "id": str(user.uuid).replace("-", ""),
                "name": user.nickname,
                "properties": [
                    {
                        "name": "textures",
                        "value": texturesb64,
                        # "signature": signature
                    },
                ],
            }
    if need_signature:
        answer["properties"][0]["signature"] = signature
    return answer

# textures = {
#         "timestamp": (datetime.datetime.now() - datetime.datetime(1970, 1, 1, 0, 0, 0)).seconds * 1000,
#         "profileId": "27b698f6e13645fa983d2823cbeb6efb",
#         "profileName": "zemlia",
#         "textures": {
#             "SKIN": {
#                 "url": "https://launcher.madness-project.ru/media/skins/771161fcfde0475f_S4YcgKT.png"
#             },
#             "CAPE": {
#                 "url": "https://launcher.madness-project.ru/media/skins/771161fcfde0475f_S4YcgKT.png"
#             }
#         }
#     }
