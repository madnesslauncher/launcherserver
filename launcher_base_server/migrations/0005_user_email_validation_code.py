# Generated by Django 4.1.4 on 2022-12-11 20:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('launcher_base_server', '0004_alter_client_zip_hash'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='email_validation_code',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
    ]
