# Generated by Django 4.1.4 on 2022-12-11 16:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('launcher_base_server', '0003_client'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='zip_hash',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Хэш конкретной версии'),
        ),
    ]
