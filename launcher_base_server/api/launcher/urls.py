from rest_framework import routers
from django.urls import path

from launcher_base_server.api.launcher import views

router = routers.SimpleRouter()
router.register(r'clients', views.ClientViewSet)

urlpatterns = router.urls + [
    path('compare-client-versions/', views.CompareClientVersionsAPIView.as_view(), name='compare_clients_versions'),
    path('get-client-download-link/', views.GetClientDownloadLinkAPIView.as_view(), name='get_client_download_link'),
]
