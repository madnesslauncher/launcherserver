from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from launcher_base_server.models import Client


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class GetClientDownloadLinkSerializer(serializers.Serializer):
    client_name = serializers.CharField(required=True, allow_null=False)

    def validate(self, attrs):
        client_name = attrs["client_name"]

        try:
            Client.objects.get(name=client_name)
        except Client.DoesNotExist:
            raise ValidationError("Client with given name does not exists")

        return attrs


class CompareClientVersionsSerializer(serializers.Serializer):
    client_name = serializers.CharField(required=True, allow_null=False)
    users_version_hash = serializers.CharField(required=True, allow_null=False)

    def validate(self, attrs):
        client_name = attrs["client_name"]

        try:
            Client.objects.get(name=client_name)
        except Client.DoesNotExist:
            raise ValidationError("Client with given name does not exists")

        return attrs

