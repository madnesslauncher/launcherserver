from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets, mixins, filters, status, permissions
from rest_framework.decorators import action

from launcher_base_server.api.launcher.serializers import ClientSerializer, CompareClientVersionsSerializer, \
    GetClientDownloadLinkSerializer
from launcher_base_server.models import Client, User


class ClientViewSet(mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]


class CompareClientVersionsAPIView(APIView):
    def post(self, request):
        serializer = CompareClientVersionsSerializer(data=request.data)
        if serializer.is_valid():
            client = Client.objects.get(name=serializer.validated_data["client_name"])
            users_version_hash = serializer.validated_data["users_version_hash"]

            if client.zip_hash != users_version_hash:
                return Response(status=status.HTTP_200_OK, data={"message": "Ok", "data": {"need_to_update": True}})
            return Response(status=status.HTTP_200_OK, data={"message": "Ok", "data": {"need_to_update": False}})
        return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)


class GetClientDownloadLinkAPIView(APIView):
    def get(self, request):
        serializer = GetClientDownloadLinkSerializer(data=request.GET)
        if serializer.is_valid():
            client = Client.objects.get(name=serializer.validated_data["client_name"])
            return Response(status=status.HTTP_200_OK, data={"message": "Ok", "data": {"link": client.client_zip.url}})
        return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)
