from rest_framework import routers
from django.urls import path

from launcher_base_server.api.sessionserver import views

router = routers.SimpleRouter()
# router.register(r'clients', views.ClientViewSet)

urlpatterns = router.urls + [
    path('session/minecraft/profile/<str:offline_hash>', views.SessionMinecraftProfileAPIView.as_view(),
         name='compare_clients_versions'),
    path('session/minecraft/join', views.SessionMinecraftJoinAPIView.as_view(), name='join'),
    path('session/minecraft/hasJoined', views.SessionMinecraftHasJoinedAPIView.as_view(),
         name='has_joined'),
]