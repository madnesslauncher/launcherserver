import datetime

from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError

from launcher_base_server.models import User, OpenedSession


class SessionMinecraftProfileSerializer(serializers.Serializer):
    offline_hash = serializers.CharField(required=True, allow_null=False)

    def validate(self, attrs):
        offline_hash = attrs["offline_hash"]
        user = None
        users = User.objects.filter(is_active=True)
        for cur_user in users:
            if cur_user.offline_hash == offline_hash or str(cur_user.uuid).replace("-", "") == offline_hash:
                user = cur_user
                break
        if not user:
            raise ValidationError("Client with given uuid does not exists")
        attrs["user"] = user
        return attrs


class SessionMinecraftJoinSerializer(serializers.Serializer):
    accessToken = serializers.CharField(required=True)
    selectedProfile = serializers.CharField(required=True)
    serverId = serializers.CharField(required=True)

    def validate(self, attrs):
        access_token = attrs.get("accessToken")
        server_id = attrs.get("serverId")
        try:
            user = Token.objects.get(key=access_token).user
        except Token.DoesNotExist:
            raise ValidationError("Unknown user")

        try:
            os = OpenedSession.objects.get(user=user, server_id=server_id)
            if os.create_datetime > datetime.datetime.now() - datetime.timedelta(seconds=30):
                raise ValidationError("Can not open new session")
            else:
                os.delete()
        except:
            print("No opened sessions")

        return attrs

