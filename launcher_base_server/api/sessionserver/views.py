"""

"""
import datetime

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from launcher_base_server.api.sessionserver.serializers import SessionMinecraftProfileSerializer, \
    SessionMinecraftJoinSerializer
from launcher_base_server.models import User, OpenedSession
from launcher_base_server.utils import generate_info_about_user

"""
/book/<book_id:int>
"""


class SessionMinecraftProfileAPIView(APIView):
    permission_classes = []

    def get(self, request, offline_hash):
        serializer = SessionMinecraftProfileSerializer(data={"offline_hash": offline_hash})
        if serializer.is_valid():
            user = serializer.validated_data["user"]
            user.last_game_enter = datetime.datetime.now()
            user.save()
            return Response(status=status.HTTP_200_OK, data=generate_info_about_user(user, request))
        print(serializer.errors)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)


class SessionMinecraftJoinAPIView(APIView):
    permission_classes = []

    def post(self, request):
        serializer = SessionMinecraftJoinSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)
        user = Token.objects.get(key=serializer.validated_data["accessToken"]).user
        session = OpenedSession(
            user=user,
            server_id=serializer.validated_data["serverId"]
        )
        session.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class SessionMinecraftHasJoinedAPIView(APIView):
    permission_classes = []

    def get(self, request):
        server_id = request.GET.get("serverId")
        username = request.GET.get("username")
        if not server_id:
            return Response(status=status.HTTP_204_NO_CONTENT)
        try:
            user = User.objects.get(nickname=username)
            user.last_game_enter = datetime.datetime.now()
            user.save()
            OpenedSession.objects.get(server_id=server_id, user=user)
            return Response(status=status.HTTP_200_OK, data=generate_info_about_user(user, request))
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)


