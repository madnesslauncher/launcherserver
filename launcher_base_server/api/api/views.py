from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from launcher_base_server.models import User
from launcher_base_server.utils import generate_info_about_user


class ProfilesMinecraftAPIView(APIView):
    permission_classes = []

    def post(self, request):
        print(type(request.data))
        answer = []
        if type(request.data) == list:
            for item in request.data:
                try:
                    print(item)
                    user = User.objects.get(nickname=item)
                except User.DoesNotExist:
                    continue
                answer.append(generate_info_about_user(user, request, True))

        return Response(status=status.HTTP_200_OK, data=answer)


        # serializer = CompareClientVersionsSerializer(data=request.data)
        # if serializer.is_valid():
        #     client = Client.objects.get(name=serializer.validated_data["client_name"])
        #     users_version_hash = serializer.validated_data["users_version_hash"]
        #
        #     if client.zip_hash != users_version_hash:
        #         return Response(status=status.HTTP_200_OK, data={"message": "Ok", "data": {"need_to_update": True}})
        #     return Response(status=status.HTTP_200_OK, data={"message": "Ok", "data": {"need_to_update": False}})
