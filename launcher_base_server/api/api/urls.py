from rest_framework import routers
from django.urls import path

from launcher_base_server.api.api import views

router = routers.SimpleRouter()

urlpatterns = router.urls + [
    path('profiles/minecraft', views.ProfilesMinecraftAPIView.as_view(), name='compare_clients_versions'),
]
