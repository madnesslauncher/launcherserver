from rest_framework import serializers

from launcher_base_server.models import User


class RegisterSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    def create(self, validated_data):

        user = User.objects.create_user(
            nickname=validated_data['username'],
            email=validated_data["email"],
            password=validated_data['password'],
        )

        return user

    class Meta:
        model = User
        # Tuple of serialized model fields (see link [2])
        fields = ("id", "nickname", "email", "password", )


class LoginSerializer(serializers.Serializer):
    nickname = serializers.CharField(max_length=255, required=True, allow_null=False)
    password = serializers.CharField(max_length=255, required=True, allow_null=False)
