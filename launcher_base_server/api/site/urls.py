from rest_framework import routers
from django.urls import path

from launcher_base_server.api.site import views

router = routers.SimpleRouter()
# router.register(r'clients', views.ClientViewSet)

urlpatterns = router.urls + [
    path('register/', views.RegisterUserView.as_view(), name='register'),
    # TODO email validation code endpoint
    path('login/', views.LoginApiView.as_view(), name='login'),
    path('get-item/', views.GetItemApiView.as_view(), name='get_item'),
]
