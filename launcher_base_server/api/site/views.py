from django.contrib.auth import authenticate
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.authtoken.models import Token
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets, mixins, filters, status, permissions
from rest_framework.decorators import action

from launcher_base_server.api.site.serializers import RegisterSerializer, LoginSerializer
from launcher_base_server.models import User
from launcher_base_server.utils import generate_info_about_user
from mcrcon import MCRcon as r


class RegisterUserView(CreateAPIView):

    model = User()
    permission_classes = [
        permissions.AllowAny,
    ]
    serializer_class = RegisterSerializer


class LoginApiView(APIView):
    permission_classes = []

    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)
        user = authenticate(nickname=serializer.validated_data["nickname"],
                            password=serializer.validated_data["password"])

        if user is not None:
            if user.is_active:
                token, created = Token.objects.get_or_create(user=user)

                return Response(status=status.HTTP_200_OK, data={
                    "message": "Ok",
                    "token": token.key,
                    "profile": generate_info_about_user(user, request)
                })
            else:
                return Response(status=status.HTTP_403_FORBIDDEN, data={"message": "Error",
                                                                        "data": {"message": "Activate account"}})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                "message": "Error",
                "data": {"message": "Wrong username or password"}
            })


class GetItemApiView(APIView):
    permission_classes = []
    def post(self, request):
        with r('localhost', 'insertyourpasswordhere') as mcr:
            resp = mcr.command('/list')
        print(resp)
