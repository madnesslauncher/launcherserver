from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.template import loader
import requests

from launcher_base_server.forms import LoginForm, RegisterForm, SkinForm, CloakForm
from launcher_base_server import settings
from launcher_base_server.models import User, Post, Server, Launcher


def get_meta_info(request):
    return JsonResponse(
        {
            "meta": {
                "implementationName": "yggdrasil-mock-server",
                "implementationVersion": "0.0.1",
                "serverName": "yushijinhun's Example Authentication Server",
                "links": {
                    "homepage": "http://127.0.0.1:8000/",
                    "register": "http://127.0.0.1:8000/register"
                },
                "feature.non_email_login": True,
                # "feature.legacy_skin_api": True
            },
            "skinDomains": settings.SKIN_DOMAINS,
            "signaturePublickey": settings.signature_public_key
        }
    )


def login_view(request):
    if request.user:
        if not request.user.is_anonymous:
            return HttpResponseRedirect('/')

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(nickname=data["nickname"], password=data["password"])
            if not user:
                return HttpResponseRedirect('/login')
            if not user.is_active:
                return HttpResponseRedirect('/login')
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/login')

    template = loader.get_template('minesite/login.html')
    return HttpResponse(template.render({}, request))


def logout_view(request):
    if not request.user.is_anonymous:
        logout(request)
    return HttpResponseRedirect('/')


def register_view(request):

    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid():
            data = form.cleaned_data
            User.objects.create_user(data["nickname"], email=data["email"], password=data["password"])
            return HttpResponseRedirect('/login')
        else:
            print(form.errors)
            return HttpResponseRedirect('/register')

    template = loader.get_template('minesite/register.html')
    return HttpResponse(template.render({}, request))


def index_view(request):
    template = loader.get_template('minesite/index.html')
    posts = []
    for item in Post.objects.all().order_by("-created_at"):
        posts.append({
            "image": item.image.url if item.image else None,
            "header": item.header,
            "text": item.text,
            "creator": item.created_by.nickname,
            "created_at": item.created_at.strftime("%d.%m.%Y в %H:%M")
        })

    servers = []

    for item in Server.objects.all():
        response = requests.get(f"https://mcapi.us/server/status?ip={item.host}&port={item.port}").json()

        servers.append({
            "name": item.name,
            "players_max": response["players"]["max"],
            "players_now": response["players"]["now"],
            "host": item.host,
            "port": item.port,
            "online": response["online"]
        })

    payload = {
        "posts": posts,
        "servers": servers
    }

    return HttpResponse(template.render(payload, request))


def profile_view(request):
    if request.user.is_anonymous:
        return HttpResponseRedirect('/login')
    template = loader.get_template('minesite/profile.html')
    return HttpResponse(template.render({}, request))


def upload_skin(request):
    if request.user.is_anonymous:
        return HttpResponseRedirect('/login')
    if request.method == 'POST':
        form = SkinForm(request.POST, instance=request.user)
        if form.is_valid():
            upload = request.FILES['skin']
            request.user.skin = upload
            request.user.save()
        else:
            print(form.errors)
    return HttpResponseRedirect('/profile')


def upload_cloak(request):
    if request.user.is_anonymous:
        return HttpResponseRedirect('/login')
    if request.method == 'POST':
        form = CloakForm(data=request.POST, instance=request.user)
        if form.is_valid():
            upload = request.FILES['cloak']
            request.user.cloak = upload
            request.user.save()
        else:
            print(form.errors)
    return HttpResponseRedirect('/profile')


def download_launcher_view(request):
    template = loader.get_template('minesite/download_client.html')
    download_launcher_link = Launcher.objects.all().order_by("-created_at").first().launcher_file.url
    return HttpResponse(template.render({"download_launcher_link": download_launcher_link}, request))
