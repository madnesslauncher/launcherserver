import hashlib
import random
import string

import uuid
from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.hashers import make_password
from django.db import models
from django.apps import apps
from django.core.mail import send_mail
from django.utils import timezone

from launcher_base_server.utils import construct_offline_player_uuid

# INCOMING = "money_incoming"
# SPENDING = "money_spending"
#
# TRANSACTION_TYPES = (
#     (INCOMING, "Пополнение"),
#     (SPENDING, "Расход"),
# )


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, nickname, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not nickname:
            raise ValueError("The given username must be set")
        email = self.normalize_email(email)
        GlobalUserModel = apps.get_model(
            self.model._meta.app_label, self.model._meta.object_name
        )
        username = GlobalUserModel.normalize_username(nickname)
        user = self.model(nickname=username, email=email, **extra_fields)
        user.password = make_password(password)
        if settings.USE_EMAIL_VALIDATION:
            user.is_active = False
            validation_code = "".join(random.sample(string.digits + string.ascii_letters, 8))
            user.email_validation_code = validation_code
            send_mail(
                'Launcher registration validation',
                f'Hi! Your registration code is {validation_code}',
                settings.EMAIL_HOST_USER,
                [email],
                fail_silently=False,
            )

        user.save(using=self._db)
        return user

    def create_user(self, nickname, email=None, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(nickname, email, password, **extra_fields)

    def create_superuser(self, nickname, email=None, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(nickname, email, password, **extra_fields)


class User(AbstractBaseUser):
    username = None
    nickname = models.CharField(max_length=255, verbose_name="Имя пользователя", unique=True)
    email = models.EmailField('Email', unique=True)

    skin = models.ImageField(verbose_name="Скин", null=True, blank=True,
                             upload_to='skins/')

    cloak = models.ImageField(verbose_name="Плащ", null=True, blank=True,
                              upload_to='cloaks/')

    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    uuid = models.UUIDField(default=uuid.uuid4, null=False, blank=False, verbose_name="UUID", unique=True)
    email_validation_code = models.CharField(max_length=6, null=True, blank=True)

    created_at = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True, null=False, blank=False)
    last_game_enter = models.DateTimeField(verbose_name="Дата последнего входа в игру", null=True, blank=True)

    @property
    def offline_hash(self):
        return construct_offline_player_uuid(self.nickname)

    @property
    def balance(self):
        balance = 0
        for transaction in self.transactions.all():
            balance += transaction.amount
        return balance

    USERNAME_FIELD = 'nickname'
    REQUIRED_FIELDS = ['email']

    objects = CustomUserManager()

    def has_perm(self, perm, obj=None):
        """Does the user have a specific permission?"""
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        """Does the user have permissions to view the app `app_label`?"""
        # Simplest possible answer: Yes, always
        return True

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Client(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название клиента")
    client_zip = models.FileField(verbose_name="Zip файл", null=True, blank=True, upload_to='clients/',
                                  help_text="Zip файл с клиентом (Внутри должна лежать директория, которая копирует"
                                            " название клинта но с точкой веперди, например: если название клиента"
                                            " HI-TECH, то в архиве лежит папка .HI-TECH)")
    zip_hash = models.CharField(max_length=255, verbose_name="Хэш конкретной версии", null=True, blank=True)

    def __str__(self):
        return self.name

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        super().save(force_insert=False, force_update=False, using=None, update_fields=None)
        with open(f"{self.client_zip.path}", 'rb') as f:
            self.zip_hash = hashlib.sha256(f.read()).hexdigest()
        super().save(force_insert=False, force_update=False, using=None, update_fields=None)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class OpenedSession(models.Model):
    server_id = models.CharField(max_length=255, null=False, blank=False)
    user = models.ForeignKey("User", on_delete=models.CASCADE, related_name="sessions", null=False, blank=False)
    create_datetime = models.DateTimeField(null=False, blank=False, default=timezone.now())


class Post(models.Model):
    image = models.ImageField(verbose_name="Изображение", null=True, blank=True,
                              upload_to='post_images/')
    header = models.CharField(max_length=255, null=False, blank=False, verbose_name="Заголовок статьи")
    text = models.TextField(null=False, blank=False, verbose_name="Текст статьи")
    created_at = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)
    created_by = models.ForeignKey("User", on_delete=models.SET_NULL, related_name="posts", null=True, blank=False,
                                   verbose_name="Создавший пользователь")

    def __str__(self):
        return self.header if len(self.header) < 100 else self.header[0: 97] + "..."

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'


class Server(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False, verbose_name="Имя сервера")
    host = models.CharField(max_length=255, null=False, blank=False, verbose_name="Хост сервера",
                            help_text="Например: 95.79.50.50 или play.madness-project.ru")
    port = models.IntegerField(null=False, blank=False, verbose_name="Порт сервера")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Сервер'
        verbose_name_plural = 'Серверы'


class Transaction(models.Model):
    user = models.ForeignKey("User", on_delete=models.CASCADE, related_name='transactions', null=False, blank=False,
                             verbose_name="Пользователь")
    amount = models.FloatField(null=False, blank=False, verbose_name="Операция")

    def __str__(self):
        return f"{self.user.nickname} {self.amount}"

    class Meta:
        verbose_name = 'Транзакция'
        verbose_name_plural = 'Транзакции'


class Launcher(models.Model):
    launcher_file = models.FileField(verbose_name="Zip или exe файл", null=True, blank=True, upload_to='launchers/')
    launcher_file_hash = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(null=False, blank=False, auto_now_add=True)

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        super().save(force_insert=False, force_update=False, using=None, update_fields=None)
        with open(f"{self.launcher_file.path}", 'rb') as f:
            self.launcher_file_hash = hashlib.sha256(f.read()).hexdigest()
        super().save(force_insert=False, force_update=False, using=None, update_fields=None)

    def __str__(self):
        return f"{self.created_at}"

    class Meta:
        verbose_name = 'Лаунчер'
        verbose_name_plural = 'Лаунчеры'
