from django import forms
from django.core.exceptions import ValidationError

from launcher_base_server.models import User


class LoginForm(forms.Form):
    nickname = forms.CharField(label='', max_length=255)
    password = forms.CharField(label='', max_length=255)


class RegisterForm(forms.Form):
    nickname = forms.CharField(label='', max_length=255)
    password = forms.CharField(label='', max_length=255)
    email = forms.EmailField(label='', max_length=255)
    password_repeat = forms.CharField(label='', max_length=255)

    def clean(self):
        super().clean()
        print(1)
        data = self.cleaned_data

        try:
            User.objects.get(nickname=data["nickname"])
        except:
            pass
        else:
            raise ValidationError("User with given nickname already exists")

        try:
            User.objects.get(nickname=data["email"])
        except:
            pass
        else:
            raise ValidationError("User with given email already exists")

        if data["password"] != data["password_repeat"]:
            raise ValidationError("Passwords did not match")

        return data


class SkinForm(forms.ModelForm):
    """Form for the image model"""
    class Meta:
        model = User
        fields = ('skin', )


class CloakForm(forms.ModelForm):
    """Form for the image model"""
    class Meta:
        model = User
        fields = ('cloak', )
