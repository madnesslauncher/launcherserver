"""launcher_base_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from launcher_base_server.api.launcher.urls import urlpatterns as launcher_api_url_patterns
from launcher_base_server.api.site.urls import urlpatterns as site_api_url_patterns
from launcher_base_server.api.sessionserver.urls import urlpatterns as sessionserver_api_url_patterns
from launcher_base_server.api.api.urls import urlpatterns as api_api_url_patterns
from launcher_base_server.views import get_meta_info, login_view, logout_view, register_view, index_view,\
    profile_view, upload_skin, upload_cloak, download_launcher_view

urlpatterns = [
    path('', index_view),
    path('login/', login_view),
    path('logout/', logout_view),
    path('register/', register_view),
    path('profile/', profile_view),
    path('mineapi/', get_meta_info),
    path('download-launcher/', download_launcher_view),
    path('upload-skin/', upload_skin),
    path('upload-cloak/', upload_cloak),
    path('admin/', admin.site.urls),
    re_path(r'^mineapi/launcher/', include(launcher_api_url_patterns)),
    re_path(r'^mineapi/site/', include(site_api_url_patterns)),
    re_path(r'^mineapi/sessionserver/', include(sessionserver_api_url_patterns)),
    re_path(r'^mineapi/api/', include(api_api_url_patterns)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
