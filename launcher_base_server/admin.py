from django.contrib import admin

from launcher_base_server.models import User, Client, Post, Server, Launcher

admin.site.register(User)
admin.site.register(Post)
admin.site.register(Server)
admin.site.register(Client)
admin.site.register(Launcher)
